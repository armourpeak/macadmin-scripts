#!/bin/sh

# 2019 ARMOUR PEAK LLC
# A simple example of how to encode a file (icon, png, ini, conf, etc) into a script

# Encode:
openssl base64 -in /path/to/file.icns

# Decode:

ICON_NAME="mojave_install.png"

DEST=/Library/Application\ Support/ArmourPeak/Icons/
mkdir -p $DEST

openssl base64 -d <<ICON > $DEST$ICON_NAME
#<<paste your data here>>
ICON
